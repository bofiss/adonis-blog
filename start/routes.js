'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

Route.get('/', 'HomeController.index')
Route.get('/posts/:id', 'HomeController.post')
Route.get('/posts', 'HomeController.all')
Route.get('/contact', 'HomeController.contact')
Route.get('/about', 'HomeController.about')
Route.get('/post/create', 'HomeController.create')
Route.post('/post/create', 'HomeController.store')
Route.on('/auth/signup').render('auth.sign-up')
Route.post('/auth/signup', 'UserController.store')
Route.on('/auth/signin').render('auth.sign-in')
Route.post('/auth/signin', 'UserController.store')