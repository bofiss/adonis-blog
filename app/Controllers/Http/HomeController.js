'use strict'

const Post = use('App/Models/Post')

class HomeController {
    
    async index({view}) {
        const posts = await Post.all()
        return view.render('blogs.index', { title: 'AdonisJS 4.0 Blog', posts: posts.toJSON()})
    }

    async about({view}) {
        return view.render('blogs.about')
    }

    async post({view, params}) {
      const post = await Post.find(params.id)
      return view.render('blogs.post', { post: post.toJSON()})
    }

    async create({view, request, response}) {
        return view.render('blogs.create')
    }

    async edit({view, request }) {

        const { post } = request
        return view.render('blogs.edit', {post: post})

    }

    async update({response, request, params, session }) {

        const post = new Post()
        post.title = request.input('title')
        post.subtitle = request.input('subtitle')
        post.body = request.input('message')
         
        await post.save()
        session.flash({notification: 'Post updated!'})

        return response.redirect('/')
    }


    async destroy({ request, params, response, session }) {

        const { post } = request
        post.delete()
        session.flash({notification: 'Post deleted successfully!'})

        return response.redirect('/')
    }

    async store({request, response, session}) {
        const post = new Post()
        post.title = request.input('title')
        post.subtitle = request.input('subtitle')
        post.body = request.input('message')

        await post.save()
        session.flash({notification: 'Post Added!'})

        return response.redirect('/')

    }

}

module.exports = HomeController
