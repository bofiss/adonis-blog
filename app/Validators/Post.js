'use strict'

class Post {
  get rules () {
    return {
      title: 'required',
      subtitle: 'required',
      message: 'required'
    }
  }

  get messages () {
    return {
      'title.required': 'Please provide a title',
      'subtitle.required': 'Please provide a subtitle',
      'message.required': 'Please provide a message'
    }
  }

  async fails (errorMessages) {
    this.ctx.session.withErrors(errorMessages).flashAll()
    return this.ctx.response.redirect('back')

  }

}

module.exports = Post
