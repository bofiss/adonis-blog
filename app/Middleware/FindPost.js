'use strict'

const Post = use('App/Models/Post')


class FindPost {

  async handle ({ response, request, params, session }, next) {
  
    const post = await Post.find(params.id)
    
    if (!post) {
      session.flash({notification: 'Post was not found'})
      return response.redirect('/')
    }

    request.post = post

    await next()
  }
}




module.exports = FindPost
