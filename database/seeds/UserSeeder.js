'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database')

class UserSeeder {
  async run () {
    const user = await Factory.model('App/Models/User').create()
    const post = await Factory.model('App/Models/Post').create()
    
    await user.posts().save(post)
      const users = await Database.table('users')
      console.log(users)
    }
}

module.exports = UserSeeder
